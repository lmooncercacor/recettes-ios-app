//
//  CalendarSectionHeaderView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-15.
//

import UIKit
import JTAppleCalendar

class CalendarSectionHeaderView: JTACMonthReusableView {

    @IBOutlet weak var title: UILabel!
}
