//
//  CurrentCellView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-15.
//

import UIKit
import JTAppleCalendar

class CurrentMonthCellView: JTACDayCell {

    @IBOutlet weak var selectedView: UIView!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
}
