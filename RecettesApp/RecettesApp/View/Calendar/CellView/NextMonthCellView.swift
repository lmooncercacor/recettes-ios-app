//
//  NextMonthCellView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-16.
//

import UIKit
import JTAppleCalendar

class NextMonthCellView: JTACDayCell {
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!


}
