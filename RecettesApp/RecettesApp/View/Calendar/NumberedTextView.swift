//
//  NumberedTextView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-22.
//

import UIKit

class NumberedTextView: UITextView {
    override func willMove(toSuperview newSuperview: UIView?) {
        frame = newSuperview?.frame.insetBy(dx: 50, dy: 80) ?? frame
        backgroundColor = .clear
        NotificationCenter.default.addObserver(self, selector: #selector(textViewDidChange), name: UITextView.textDidChangeNotification, object: nil)
    }
    @objc func textViewDidChange(notification: Notification) {
        var lines: [String] = []
        for (index, line) in text.components(separatedBy: .newlines).enumerated() {
            if !line.hasPrefix("\(index.advanced(by: 1))") &&
                !line.trimmingCharacters(in: .whitespaces).isEmpty {
                lines.append("\(index.advanced(by: 1)). " + line)
            } else {
                lines.append(line)
            }
        }
        text = lines.joined(separator: "\n")
        // this prevents two empty lines at the bottom
        if text.hasSuffix("\n\n") {
            text = String(text.dropLast())
        }
    }
}
