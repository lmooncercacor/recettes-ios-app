//
//  MenuItemView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-23.
//

import UIKit

class MenuItemView: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var menuItemNameView: UIView! {
        didSet {
            menuItemNameView.layer.borderWidth = 1.0
            menuItemNameView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var menuItemCuisineView: UIView! {
        didSet {
            menuItemCuisineView.layer.borderWidth = 1.0
            menuItemCuisineView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var menuItemPriceView: UIView! {
        didSet {
            menuItemPriceView.layer.borderWidth = 1.0
            menuItemPriceView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var menuItemDescriptionView: UIView! {
        didSet {
            menuItemDescriptionView.layer.borderWidth = 1.0
            menuItemDescriptionView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var menuItemNameLabel: UILabel!
    @IBOutlet weak var menuItemCuisineLabel: UILabel!
    @IBOutlet weak var menuItemPriceLabel: UILabel!
    @IBOutlet weak var menuItemDescriptionTextView: UITextView!

    @IBOutlet weak var menuItemNameEditButton: UIButton! {
        didSet {
            menuItemNameEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var menuItemCuisineEditButton: UIButton! {
        didSet {
            menuItemCuisineEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var menuItemPriceEditButton: UIButton! {
        didSet {
            menuItemPriceEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var menuItemDescriptionEditButton: UIButton! {
        didSet {
            menuItemDescriptionEditButton.setTitle("", for: .normal)
        }
    }
    
    var menuItemNameEditButtonTapped: (UIButton) -> Void = { _ in }
    var menuItemCuisineEditButtonTapped: (UIButton) -> Void = { _ in }
    var menuItemPriceEditButtonTapped: (UIButton) -> Void = { _ in }
    var menuItemDescriptionEditButtonTapped: (UIButton) -> Void = { _ in }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MenuItemView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    @IBAction func menuItemNameEditButtonAction(_ sender: UIButton) {
        menuItemNameEditButtonTapped(sender)
    }
    
    @IBAction func menuItemCuisineEditButtonAction(_ sender: UIButton) {
        menuItemCuisineEditButtonTapped(sender)
    }
    
    @IBAction func menuItemPriceEditButtonAction(_ sender: UIButton) {
        menuItemPriceEditButtonTapped(sender)
    }
    
    @IBAction func menuItemDescriptionEditButtonAction(_ sender: UIButton) {
        menuItemDescriptionEditButtonTapped(sender)
    }
}
