//
//  RecipeTableViewCell.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-25.
//
import UIKit

class RecipeTableViewCell: UITableViewCell {
    @IBOutlet weak var recipeNameLabel: UILabel!
}
