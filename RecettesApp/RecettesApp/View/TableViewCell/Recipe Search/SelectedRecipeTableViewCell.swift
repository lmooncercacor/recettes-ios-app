//
//  SelectedRecipeTableViewCell.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-01-10.
//

import UIKit

class SelectedRecipeTableViewCell: UITableViewCell {
    @IBOutlet weak var recipeNameLabel: UILabel!
}
