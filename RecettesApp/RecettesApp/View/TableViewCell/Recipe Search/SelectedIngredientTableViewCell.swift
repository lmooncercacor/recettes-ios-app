//
//  SelectedIngredientTableViewCell.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-01-21.
//

import UIKit

protocol FoodTableViewCellDelegate: AnyObject {
    func quantifyCounter(cell: SelectedIngredientTableViewCell)
    func unitSelector(cell: SelectedIngredientTableViewCell, unit: String)
}

class SelectedIngredientTableViewCell: UITableViewCell {
    weak var delegate: FoodTableViewCellDelegate?
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var quantityCounter: UIStepper!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var unitDropDown: iOSDropDown!
    @IBOutlet weak var ingredientNameLabel: UILabel!
    @IBOutlet weak var ingredientImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        quantityCounter.transform = CGAffineTransform(scaleX: 0.75, y: 1.0)
        quantityCounter.autorepeat = true
        quantityCounter.maximumValue = 100
        quantityCounter.minimumValue = 0
        unitDropDown.optionArray = ["g", "kg", "ml", "ounce", "pound", "1 tsp", "3/4 tsp", "1/2 tsp", "1/3 tsp", "1/4 tsp", "1/8 tsp", "1 tsp", "1 cup", "1/2 cup", "1/3 cup", "1/4 cup"]
        unitDropDown.optionIds = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        unitDropDown.didSelect{(selectedText , index ,id) in
            self.unitLabel.text = "\(selectedText)"
            self.delegate?.unitSelector(cell: self, unit: selectedText)
        }
    }
    
    @IBAction func quantityChangeButtonTapped(_ sender: Any) {
        delegate?.quantifyCounter(cell: self)
        quantityLabel.text = "\(Int(quantityCounter.value))"
    }
}
