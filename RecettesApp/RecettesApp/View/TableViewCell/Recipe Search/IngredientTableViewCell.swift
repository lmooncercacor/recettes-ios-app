//
//  IngredientTableViewCell.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-12-10.
//

import UIKit

class IngredientTableViewCell: UITableViewCell {
    @IBOutlet weak var quantityLabel: UILabel! {
        didSet {
            quantityLabel.layer.borderWidth = 1.0
            quantityLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var unitLabel: UILabel! {
        didSet {
            unitLabel.layer.borderWidth = 1.0
            unitLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var ingredientLabel: UILabel! {
        didSet {
            ingredientLabel.layer.borderWidth = 1.0
            ingredientLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
}
