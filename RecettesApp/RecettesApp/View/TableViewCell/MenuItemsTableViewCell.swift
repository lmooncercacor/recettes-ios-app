//
//  MenuItemsTableViewCell.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-16.
//

import UIKit

protocol MenuItemsTableViewCellDelegate: AnyObject {
    func addRemoveButtonTapped(cell: MenuItemsTableViewCell)
}

class MenuItemsTableViewCell: UITableViewCell {
    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var menuAddRemoveButton: UIButton! {
        didSet {
            menuAddRemoveButton.layer.cornerRadius = 8.0
        }
    }
    weak var delegate: MenuItemsTableViewCellDelegate?

    @IBAction func menuAddRemoveButtonAction(_ sender: Any) {
        delegate?.addRemoveButtonTapped(cell: self)
    }
}
