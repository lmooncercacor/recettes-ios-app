//
//  NutritionFactsView.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-08.
//

import UIKit

class NutritionFactsView: UIView {
    @IBOutlet weak var servingSize: UILabel!
    @IBOutlet weak var calorie: UILabel!
    @IBOutlet weak var totalFat: UILabel!
    @IBOutlet weak var saturatedFat: UILabel!
    @IBOutlet weak var transFat: UILabel!
    @IBOutlet weak var cholesterol: UILabel!
    @IBOutlet weak var sodium: UILabel!
    @IBOutlet weak var potassium: UILabel!
    @IBOutlet weak var totalCarbohydrates: UILabel!
    @IBOutlet weak var dietaryFibre: UILabel!
    @IBOutlet weak var sugars: UILabel!
    @IBOutlet weak var protein: UILabel!
    @IBOutlet var contentView: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("NutritionFactsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
}
