//
//  IngredientRequestHandler.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-03-16.
//

import Foundation

enum DataLoadingErrors: Error, CustomStringConvertible {
    case invalidEndPoint(String)
    case invalidURL(String)
    case nonHTTPResponse
    case requestFailed(Int)
    case networkingError(Error)
    
    var description: String {
        switch self {
        case .invalidEndPoint(let str):
            return "Invalid End Point: " + str
        case .invalidURL(let str):
            return "Invalid URL: " + str
        case .nonHTTPResponse:
            return "Non HTTP URL Response Received"
        case .requestFailed(let status):
            return "Received HTTP \(status)"
        case .networkingError(let error):
            return "Networking error: \(error.localizedDescription)"
        }
    }
}

class IngredientRequestHandler {
    private let decoder = JSONDecoder()

    static let appId = "1803cfb6"
    static let appKey = "9915ab3bae5df3cefbdbb150c33f4e39"
    private let searchQueryItems: [URLQueryItem] = [URLQueryItem(name: "query", value: "{SEARCH_TERM}"),
                                                    URLQueryItem(name: "branded_type", value: "1")]
    static let foodItemURL = URL(string: "https://trackapi.nutritionix.com/v2/search/instant")
    static let brandedItemNutritionURL = URL(string: "https://trackapi.nutritionix.com/v2/search/item")
    static let commonFoodNutritionURL = URL(string: "https://trackapi.nutritionix.com/v2/natural/nutrients")
    @Published public private(set) var foodSearchResults: NutritionixFoodData = NutritionixFoodData(common: [], branded: [])
    @Published public private(set) var nutritionResults: FoodNutritionData = FoodNutritionData(foods: [])
    private let brandedNutritionQueryItems: [URLQueryItem] = [URLQueryItem(name: "nix_item_id", value: "{NIX_ITEM_ID}")]
    
    private func configureCommonFoodNutritionRequest(using url: URL?, data: [String: String]) -> URLRequest? {
        guard let nURL = url else { return nil }
        do {
            var req = URLRequest(url: nURL)
            req.httpMethod = "POST"
            req.httpBody = try JSONEncoder().encode(data)
            req.setValue("application/json", forHTTPHeaderField: "Content-Type")
            req.setValue(Self.appId, forHTTPHeaderField: "x-app-id")
            req.setValue(Self.appKey, forHTTPHeaderField: "x-app-key")
            return req
        } catch let e {
            print(e.localizedDescription)
            return nil
        }
    }
    
    func loadCommonFoodNutritionData(_ commonFoodData: [String: String], completion: @escaping (_ results: FoodNutritionData) -> ()) {
        guard let req = configureCommonFoodNutritionRequest(using: Self.commonFoodNutritionURL, data: commonFoodData) else { return }
        let task = URLSession.shared.dataTask(with: req) { [self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                nutritionResults = try self.decoder.decode(FoodNutritionData.self, from: data)
            } catch let e {
                print(e.localizedDescription)
            }
            completion(nutritionResults)
        }
        task.resume()
    }
    
    private func configureBrandedFoodNutritionRequest(using url: URL?, nixItemId: String) -> URLRequest? {
        guard let nURL = url, var urlComps = URLComponents(string: nURL.absoluteString) else { return nil }
        urlComps.queryItems = brandedNutritionQueryItems
            .map({ urlQueryItem in
                let value = urlQueryItem.value?.replacingOccurrences(of: "{NIX_ITEM_ID}", with: nixItemId)
                return URLQueryItem(name: urlQueryItem.name, value: value)
            })
        guard let nutritionURL = urlComps.url else { return nil }
        var req = URLRequest(url: nutritionURL)
        req.setValue(Self.appId, forHTTPHeaderField: "x-app-id")
        req.setValue(Self.appKey, forHTTPHeaderField: "x-app-key")
        return req
    }
    
    func loadBrandedFoodNutritionData(_ nixItemId: String, completion: @escaping (_ results: FoodNutritionData) -> ()) {
        guard let req = configureBrandedFoodNutritionRequest(using: Self.brandedItemNutritionURL, nixItemId: nixItemId) else { return }
        let task = URLSession.shared.dataTask(with: req) { [self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            do {
                nutritionResults = try self.decoder.decode(FoodNutritionData.self, from: data)
            } catch let e {
                print(e.localizedDescription)
            }
            print("nutritionResults: \(nutritionResults)")
            completion(nutritionResults)
        }
        task.resume()
    }
    
    func loadFoodList(using searchText: String, completion: @escaping (_ results: NutritionixFoodData) -> ()) {
        guard !searchText.isEmpty else {
            foodSearchResults = NutritionixFoodData(common: [], branded: [])
            return
        }
        guard let req = configureFoodSearchRequest(using: Self.foodItemURL, searchText: searchText) else { return }
        let task = URLSession.shared.dataTask(with: req) { [self] data, _, error in
            guard let data = data, error == nil else {
                return
            }
            let str = String(data: data, encoding: .utf8) ?? ""
            let filteredStr = str.split(separator: "\n").filter({!$0.contains("\"guide\":")}).joined(separator: "\n")
            guard let strData = filteredStr.data(using: .utf8) else { return }
            do {
                foodSearchResults = try self.decoder.decode(NutritionixFoodData.self, from: strData)
            } catch let e {
                print(e.localizedDescription)
            }
            completion(foodSearchResults)
        }
        task.resume()
    }
    
    private func configureFoodSearchRequest(using url: URL?, searchText: String) -> URLRequest? {
        guard let locURL = url, var urlComps = URLComponents(string: locURL.absoluteString) else { return nil }
        urlComps.queryItems = searchQueryItems
            .map({ urlQueryItem in
                let value = urlQueryItem.value?.replacingOccurrences(of: "{SEARCH_TERM}", with: searchText)
                return URLQueryItem(name: urlQueryItem.name, value: value)
            })
        guard let locationURL = urlComps.url else { return nil }
        var req = URLRequest(url: locationURL)
        req.setValue(Self.appId, forHTTPHeaderField: "x-app-id")
        req.setValue(Self.appKey, forHTTPHeaderField: "x-app-key")
        return req
    }
}
