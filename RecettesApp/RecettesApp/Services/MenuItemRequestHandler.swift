//
//  MenuItemRequestHandler.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-12-21.
//

import Foundation

enum MenuItemRequestHandlerKey {
    case success(Int)
    case failure(String)
}

class MenuItemRequestHandler {
    typealias completionBlock = (Result<[MenuItems], Error>) -> Void
    //    private let baseURL = "https://delta.guava.ccc-jaguar-nugv.co"
    private let baseURL = "https://gamma.guava.cercacor.com"
    
    func retrieveAllMenuItems(completionBlock : @escaping completionBlock) {
        let endpoint = "/restaurants/29/menuitems"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let request = URLRequest(url: url)
        JSONDecoder().decoderWithRequest([MenuItems].self, fromURLRequest: request) { (result, error) in
            if let error = error {
                completionBlock(.failure(error))
            } else {
                completionBlock(.success(result!))
            }
        }
    }
    
    func retrieveMenuItems(menuDate: String, completionBlock : @escaping completionBlock) {
        let endpoint = "/restaurants/29/menuitems?menu_date=\(menuDate)T08:00:00.000Z"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let request = URLRequest(url: url)
        JSONDecoder().decoderWithRequest([MenuItems].self, fromURLRequest: request) { (result, error) in
            if let error = error {
                completionBlock(.failure(error))
            } else {
                completionBlock(.success(result!))
            }
        }
    }
    
    func retrieveDishType(menuItemId: Int64, completionBlock : @escaping (Result<MenuItems, Error>) -> Void) {
        let id = String(menuItemId)
        let endpoint = "/restaurantItems/\(id)"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let request = URLRequest(url: url)
        JSONDecoder().decoderWithRequest(MenuItems.self, fromURLRequest: request) { (result, error) in
            if let error = error {
                completionBlock(.failure(error))
            } else {
                completionBlock(.success(result!))
            }
        }
    }
    
    func deleteMenuItems(menuId: String, menuItemId: String, completion: @escaping (_ success: Bool) -> ()) {
        let endpoint = "/restaurants/29/menus/\(menuId)/items/\(menuItemId)"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling DELETE")
                return
            }
            if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode {
                print("Success: HTTP request in deleteMenuItems")
                completion(true)
            } else {
                print("Error: HTTP request failed")
                completion(false)
            }
        }.resume()
    }
    
    func addToMenuItemPool(name: String, dishType: String, description: String, completion: @escaping (_ success: Bool, _ itemId: Int) -> ()) {
        let endpoint = "/restaurantItems"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let parameters: [String: Any] = ["restaurant_id": "29", "name": name, "description": description]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        JSONDecoder().decoderWithRequest(MenuId.self, fromURLRequest: request) { (result, error) in
            if let error = error {
                print("Error: error calling POST in addToMenuItemPool: \(error)")
            } else {
                completion(true, result!.id)
            }
        }
    }
    
    func retrieveMenuIdFor(selectedDate: String, completion: @escaping (MenuItemRequestHandlerKey) -> ()) {
        let endpoint = "/restaurants/29/menus?menu_date=\(selectedDate)T08:00:00.000Z"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        JSONDecoder().decoderWithRequest([MenuId].self, fromURLRequest: request) { (result, error) in
            if let error = error {
                print("Error: error calling GET in retrieveMenuIdFor: \(error)")
            } else {
                if let menuId = result!.first?.id {
                    completion(.success(menuId))
                }
            }
        }
    }
    
    // Create menu item to retrieve menu_id for selected date
    func createMenuIdFor(selectedDate: String, completion: @escaping (_ success: Bool) -> ()) {
        let endpoint = "/restaurants/29/menus"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let parameters: [String: Any] = ["effective_date": "\(selectedDate)T08:00:00.000Z", "expired_date": "\(selectedDate)T08:00:00.000Z"]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling POST")
                print(error!)
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            completion(true)
        }.resume()
    }
    
    // Post menu to the selected date
    func addMenuToSelectedDate(menuId: Int, price: String, itemId: Int, completion: @escaping (_ success: Bool) -> ()) {
        let endpoint = "/restaurants/29/menus/\(menuId)/items"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let parameters: [String: Any] = ["menu_id": menuId, "price": price, "item_id": itemId]
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling POST")
                print(error!)
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            completion(true)
        }.resume()
    }
    
    func retrieveRestaurantItem(restaurantItemId: Int64, completion: @escaping (MenuItemRequestHandlerKey) -> ()) {
        let endpoint = "/restaurantItems/\(restaurantItemId)"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        JSONDecoder().decoderWithRequest([MenuId].self, fromURLRequest: request) { (result, error) in
            if let error = error {
                print("Error: error calling GET in retrieveRestaurantItem: \(error)")
            } else {
                if let menuId = result!.first?.id {
                    completion(.success(menuId))
                }
            }
        }
    }
    
    func updateMenuToSelectedDate(menuId: Int, price: String, dishType: String, itemId: Int, description: String, completion: @escaping (_ success: Bool) -> ()) {
        let endpoint = "/restaurantItems/\(itemId)"
        let url = baseURL + endpoint
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        let parameters: [String: Any] = ["id": itemId, "restaurant_id": 29, "dish_type": dishType, "price": price, "description": description]
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
            return
        }
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("Error: error calling PUT")
                print(error!)
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                print("Error: HTTP request failed")
                return
            }
            completion(true)
        }.resume()
    }
}
