//
//  Collection+Extensions.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-11-30.
//
import UIKit

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
