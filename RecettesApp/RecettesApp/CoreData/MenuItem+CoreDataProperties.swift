//
//  MenuItem+CoreDataProperties.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-03.
//
//

import Foundation
import CoreData


extension MenuItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MenuItem> {
        return NSFetchRequest<MenuItem>(entityName: "MenuItem")
    }

    @NSManaged public var id: Int64
    @NSManaged public var menuItemDescription: String?
    @NSManaged public var name: String?
    @NSManaged public var price: Float
    @NSManaged public var type: String?
    @NSManaged public var calendarDate: CalendarDate?
    @NSManaged public var recipes: NSSet?

}

// MARK: Generated accessors for recipes
extension MenuItem {

    @objc(addRecipesObject:)
    @NSManaged public func addToRecipes(_ value: Recipe)

    @objc(removeRecipesObject:)
    @NSManaged public func removeFromRecipes(_ value: Recipe)

    @objc(addRecipes:)
    @NSManaged public func addToRecipes(_ values: NSSet)

    @objc(removeRecipes:)
    @NSManaged public func removeFromRecipes(_ values: NSSet)

}

extension MenuItem : Identifiable {

}
