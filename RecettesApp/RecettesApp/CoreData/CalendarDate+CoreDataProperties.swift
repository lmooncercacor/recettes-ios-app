//
//  CalendarDate+CoreDataProperties.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-03.
//
//

import Foundation
import CoreData


extension CalendarDate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CalendarDate> {
        return NSFetchRequest<CalendarDate>(entityName: "CalendarDate")
    }

    @NSManaged public var selectedDate: String?
    @NSManaged public var menuItem: NSSet?

}

// MARK: Generated accessors for menuItem
extension CalendarDate {

    @objc(addMenuItemObject:)
    @NSManaged public func addToMenuItem(_ value: MenuItem)

    @objc(removeMenuItemObject:)
    @NSManaged public func removeFromMenuItem(_ value: MenuItem)

    @objc(addMenuItem:)
    @NSManaged public func addToMenuItem(_ values: NSSet)

    @objc(removeMenuItem:)
    @NSManaged public func removeFromMenuItem(_ values: NSSet)

}

extension CalendarDate : Identifiable {

}
