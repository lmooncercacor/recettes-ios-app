//
//  Ingredient+CoreDataProperties.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-03.
//
//

import Foundation
import CoreData


extension Ingredient {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Ingredient> {
        return NSFetchRequest<Ingredient>(entityName: "Ingredient")
    }

    @NSManaged public var amount: Float
    @NSManaged public var measurement: String?
    @NSManaged public var name: String?
    @NSManaged public var imgUrl: String?
    @NSManaged public var recipe: Recipe?

}

extension Ingredient : Identifiable {

}
