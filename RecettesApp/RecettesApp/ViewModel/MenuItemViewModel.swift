//
//  MenuItemViewModel.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-12-22.
//

import Foundation

class MenuItemViewModel : NSObject {
    private var menuItemRequestHandler : MenuItemRequestHandler!
    var menuItems = [MenuItems]()
    
    override init() {
        super.init()
        self.menuItemRequestHandler = MenuItemRequestHandler()
    }
    
    func fetchAllMenuItems(completion : @escaping ([MenuItems]) -> ()) {
        menuItemRequestHandler.retrieveAllMenuItems() { [weak self] result in
            if let weakSelf = self {
                switch result {
                case .success(let menus):
                    weakSelf.menuItems = menus
                case .failure:
                    weakSelf.menuItems = []
                }
                completion(weakSelf.menuItems)
            }
        }
    }
    
    func fetchMenuItems(menuDate: String, completion : @escaping ([MenuItems]) -> ()) {
        menuItemRequestHandler.retrieveMenuItems(menuDate: menuDate) { [weak self] result in
            if let weakSelf = self {
                switch result {
                case .success(let menus):
                    weakSelf.menuItems = menus
                case .failure:
                    weakSelf.menuItems = []
                }
                completion(weakSelf.menuItems)
            }
        }
    }
    
    func fetchDishType(menuItemId: Int64, completion: @escaping (_ dishType: String) -> ()) {
        
        menuItemRequestHandler.retrieveDishType(menuItemId: menuItemId) { result in
            switch result {
            case .success(let menuItem):
                if let dishType = menuItem.dishType {
                    completion(dishType)
                }
                break
            case.failure(let errorMsg):
                print("Error message for retrieveMenuIdFor: \(errorMsg)")
                break
            }
        }
    }
    
    func deleteMenuItems(menuId: String, menuItemId: String, completion: @escaping (_ success: Bool) -> ()) {
        menuItemRequestHandler.deleteMenuItems(menuId: menuId, menuItemId: menuItemId) { (_) in
            DispatchQueue.main.async(execute: {
                completion(true)
            })
        }
    }
    
    func addToMenuItemPool(name: String, dishType: String, description: String, completion: @escaping (_ success: Bool, _ itemId: Int) -> ()) {
        menuItemRequestHandler.addToMenuItemPool(name: name, dishType: dishType, description: description, completion: { success, returnedId  in
            if success {
                completion(true, returnedId)
            } else {
                print("Error message for addToMenuItemPool")
            }
        })
    }
    
    func retrieveMenuIdFor(selectedDate: String, completion: @escaping (MenuItemRequestHandlerKey) -> ()) {
        menuItemRequestHandler.retrieveMenuIdFor(selectedDate: selectedDate, completion: { result in
            switch result {
            case .success(let menuId):
                completion(.success(menuId))
                break
            case.failure(let errorMsg):
                print("Error message for retrieveMenuIdFor: \(errorMsg)")
                break
            }
        })
    }
    
    func createMenuIdFor(selectedDate: String, completion: @escaping (_ success: Bool) -> ()) {
        menuItemRequestHandler.createMenuIdFor(selectedDate: selectedDate) { success in
            if success {
                completion(true)
            } else {
                print("Error message for createMenuIdFor")
            }
        }
        
    }
    
    func addMenuToSelectedDate(menuId: Int, price: String, itemId: Int, completion: @escaping (_ success: Bool) -> ()) {
        menuItemRequestHandler.addMenuToSelectedDate(menuId: menuId, price: price, itemId: itemId, completion: { success in
            if success {
                completion(true)
            } else {
                print("Error message for addMenuToSelectedDate")
            }
        })
    }
    
    func retrieveRestaurantItem(restaurantItemId: Int64, completion: @escaping (MenuItemRequestHandlerKey) -> ()) {
        menuItemRequestHandler.retrieveRestaurantItem(restaurantItemId: restaurantItemId) { result in
            switch result {
            case .success(let menuId):
                completion(.success(menuId))
                break
            case.failure(let errorMsg):
                print("Error message for retrieveRestaurantItem: \(errorMsg)")
                break
            }
        }
    }
    
    func updateMenuToSelectedDate(menuId: Int, price: String, dishType: String, itemId: Int, description: String, completion: @escaping (_ success: Bool) -> ()) {
        menuItemRequestHandler.updateMenuToSelectedDate(menuId: Int(truncatingIfNeeded: menuId), price: price, dishType: dishType, itemId: Int(truncatingIfNeeded: itemId), description: description, completion: { success in
            if success {
                completion(true)
            } else {
                print("Error message for updateMenuToSelectedDate")
            }
        })
    }
}
