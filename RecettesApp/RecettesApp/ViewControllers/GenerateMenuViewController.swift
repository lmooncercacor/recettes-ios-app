//
//  GenerateMenuViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-03-13.
//

import UIKit

class GenerateMenuViewController: UIViewController {
    @IBOutlet weak var menuItemView: MenuItemView!
    @IBOutlet weak var updateMenuItemButton: UIButton! {
        didSet {
            updateMenuItemButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var addRecipeToMenuItemButton: UIButton! {
        didSet {
            addRecipeToMenuItemButton.layer.cornerRadius = 8.0
        }
    }
    
    private var alertTextView = UITextView()
    var menuItem : MenuItems?
    private let viewModel = MenuItemViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if menuItem != nil {
            setupMenuItemViewData()
        } else {
            addRecipeToMenuItemButton.isHidden = true
            updateMenuItemButton.setTitle("Add New Menu Item", for: .normal)
        }
        setupMenuItemViewAction()
    }
    
    private func setupMenuItemViewData() {
        menuItemView.menuItemNameLabel.text = menuItem?.name
        if let id = menuItem?.id {
            viewModel.fetchDishType(menuItemId: id) { dishType in
                DispatchQueue.main.async {
                    self.menuItemView.menuItemCuisineLabel.text = dishType
                }
            }
        }
        menuItemView.menuItemPriceLabel.text = menuItem?.price
        menuItemView.menuItemDescriptionTextView.text = menuItem?.description
    }
    
    private func setupMenuItemViewAction() {
        menuItemView.menuItemNameEditButtonTapped = { (sender: UIButton) in
            self.displayEditablePopup(label: self.menuItemView.menuItemNameLabel, title: "Menu Item", message: "Name")
        }
        menuItemView.menuItemCuisineEditButtonTapped = { (sender: UIButton) in
            self.displayEditablePopup(label: self.menuItemView.menuItemCuisineLabel, title: "Menu Item", message: "Cuisine")
        }
        menuItemView.menuItemPriceEditButtonTapped = { (sender: UIButton) in
            self.displayEditablePopup(label: self.menuItemView.menuItemPriceLabel, title: "Menu List", message: "Price")
        }
        menuItemView.menuItemDescriptionEditButtonTapped = { (sender: UIButton) in
            if !self.menuItemView.menuItemDescriptionTextView.text.isEmpty {
                self.displayAlertWithTextView(title: "Enter menu item description", message: "\n\n\n\n\n\n\n\n", textView: self.menuItemView.menuItemDescriptionTextView)
            } else {
                self.displayAlertWithTextView(title: "Enter menu item description", message: "\n\n\n\n\n\n\n\n", textView: self.menuItemView.menuItemDescriptionTextView)
            }
        }
    }
    
    private func displayAlertWithTextView(title: String, message: String, textView: UITextView) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.autoresizesSubviews = true
        alertTextView = UITextView(frame: CGRect.zero)
        alertTextView.translatesAutoresizingMaskIntoConstraints = false
        
        alertTextView.text = textView.text
        let leadConstraint = NSLayoutConstraint(item: alert.view!, attribute: .leading, relatedBy: .equal, toItem: alertTextView, attribute: .leading, multiplier: 1.0, constant: -8.0)
        let trailConstraint = NSLayoutConstraint(item: alert.view!, attribute: .trailing, relatedBy: .equal, toItem: alertTextView, attribute: .trailing, multiplier: 1.0, constant: 8.0)
        
        let topConstraint = NSLayoutConstraint(item: alert.view!, attribute: .top, relatedBy: .equal, toItem: alertTextView, attribute: .top, multiplier: 1.0, constant: -64.0)
        let bottomConstraint = NSLayoutConstraint(item: alert.view!, attribute: .bottom, relatedBy: .equal, toItem: alertTextView, attribute: .bottom, multiplier: 1.0, constant: 64.0)
        alert.view.addSubview(alertTextView)
        NSLayoutConstraint.activate([leadConstraint, trailConstraint, topConstraint, bottomConstraint])
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
            textView.text = self.alertTextView.text
        }))
        
        // Move up UIAlertController when keyboard is present: https://stackoverflow.com/a/51098284/7536185
        alert.addTextField { field in
            field.translatesAutoresizingMaskIntoConstraints = false
            field.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        let inCntrlr =  alert.children[0].view!
        inCntrlr.removeFromSuperview()
        
        present(alert, animated: true)
    }
    
    private func displayEditablePopup(label: UILabel, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        var inputTextField: UITextField?
        
        alert.addTextField { (textField : UITextField!) -> Void in
            inputTextField = textField
            textField.text = label.text
        }
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            label.text = inputTextField?.text
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func updateMenuItem(name: String, price: String, dishType: String, description: String) {
        //        if selectedMenuItemIndexPath.count > 0 {
        guard let menuId = menuItem?.menus?.first?.menu_id else { return }
        guard let itemId = menuItem?.id else { return }
        //            self.menuItemRequestHandler.updateMenuToSelectedDate(menuId: Int(truncatingIfNeeded: menuId), price: price, dishType: dishType, itemId: Int(truncatingIfNeeded: itemId), description: description, completion: { success in
        ////                if success {
        ////                    self.fetchMenuItem()
        ////                }
        //            })
        //        } else {
        //            self.networkManager.addToMenuItemPool(name: name, dishType: dishType, description: self.menuItemView.menuItemDescriptionTextView.text, completion: { success, returnedId  in
        //                if success {
        //                    self.networkManager.createMenuIdFor(selectedDate: self.selectedDateFormat, completion: { success in
        //                        if success {
        //                            self.networkManager.retrieveMenuIdFor(selectedDate: self.selectedDateFormat, completion: { result in
        //                                switch result {
        //                                case .success(let menuId):
        //                                    self.networkManager.addMenuToSelectedDate(menuId: menuId, price: price, itemId: returnedId, completion: { success in
        //                                        if success {
        //                                            self.fetchMenuItem()
        //                                        }
        //                                    })
        //                                    break
        //                                case.failure(let errorMsg):
        //                                    print("Error message for retrieveMenuIdFor: \(errorMsg)")
        //                                    break
        //                                }
        //                            })
        //                        } else {
        //                            // TODO: Display error message
        //                        }
        //                    })
        //                } else {
        //                    // TODO: Display error message
        //                }
        //            })
        //        }
    }
    
    @IBAction func updateMenuItemAction(_ sender: Any) {
        guard let name = menuItemView.menuItemNameLabel.text else { return }
        guard let price = menuItemView.menuItemPriceLabel.text else { return }
        guard let dishType = menuItemView.menuItemCuisineLabel.text else { return }
        guard let menuItemDescription = menuItemView.menuItemDescriptionTextView.text else { return }
        if !name.isEmpty || !dishType.isEmpty || !price.isEmpty || !menuItemView.menuItemDescriptionTextView.text.isEmpty {
            let submitAction : [String: () -> Void] = [ "Submit" : { (
                self.updateMenuItem(name: name, price: price, dishType: dishType, description: menuItemDescription)
            ) }]
            let cancelAction : [String: () -> Void] = [ "Cancel" : { (
            ) }]
            let buttonActions = [submitAction, cancelAction]
            if let menuItemName = menuItemView.menuItemNameLabel.text,
               let menuItemType = menuItemView.menuItemCuisineLabel.text,
               let menuItemPrice = menuItemView.menuItemPriceLabel.text {
                self.showCustomAlertWith(name: menuItemName, type: menuItemType, price: menuItemPrice, description: menuItemView.menuItemDescriptionTextView.text, itemimage: nil, actions: buttonActions)
            }
        } else {
            // show popup the field cannot be empty
        }
    }
    
    @IBAction func addRecipeToMenuItemButtonAction(_ sender: Any) {
        let detailVC = RecipeSearchViewController.instantiateViewController()
        detailVC.selectedMenuItem = menuItem
        modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        navigationController?.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        let navController = UINavigationController(rootViewController: detailVC)
        present(navController, animated: true, completion: nil)
    }
    
    class func instantiateViewController() -> GenerateMenuViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "GenerateMenuViewController") as! GenerateMenuViewController
    }
}
