//
//  RecipeSearchViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-01-10.
//

import UIKit
import CoreData

protocol RecipeSearchViewControllerDelegate {
    func refreshData()
}

class RecipeSearchViewController: UIViewController {
    var viewDelegate: RecipeSearchViewControllerDelegate?
    
    var currentCount = 0
    var selectedMenuItem : MenuItems?
    private var selectedAllRecipeIndex = IndexPath()
    private var selectedRecipes = [Recipe]()
    private var allRecipes = [Recipe]()
    private var filtredAllRecipes = [Recipe]()
    private let viewModel = MenuItemViewModel()
    private var isSearching = false
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var recipeMenuItemLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var addToMenuButton: UIButton! {
        didSet {
            addToMenuButton.isEnabled = false
            addToMenuButton.setTitleColor(UIColor.gray, for: .disabled)
            addToMenuButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var selectedRecipeTableView: UITableView!
    @IBOutlet weak var allRecipesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        setupSelectedRecipes()
        setupAllRecipes()
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewDelegate?.refreshData()
    }
    
    private func setupSelectedRecipes() {
        selectedRecipes.removeAll()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuItem")
        if let sMenuItem = selectedMenuItem {
            recipeMenuItemLabel.text = "Recipes for \(sMenuItem.name!)"
            fetchRequest.predicate = NSPredicate(format: "name == %@", sMenuItem.name!)
        }
        let fetchedMenuItem = try! context.fetch(fetchRequest) as! [MenuItem]
        if let menuItem = fetchedMenuItem.first {
            for item in menuItem.recipes as! Set<Recipe> {
                selectedRecipes.append(item)
            }
        }
    }
    
    private func setupAllRecipes() {
        allRecipes.removeAll()
        let recipesFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Recipe")
        let recipes = try! context.fetch(recipesFetchRequest) as! [Recipe]
        for recipe in recipes {
            if recipe.name != nil {
                allRecipes.append(recipe)
            }
        }
    }
    
    @IBAction func addToMenuButtonAction(_ sender: Any) {
        var modifiedRecipe = Recipe(context: self.context)
        if isSearching {
            modifiedRecipe = filtredAllRecipes[selectedAllRecipeIndex.row]
        } else {
            modifiedRecipe = allRecipes[selectedAllRecipeIndex.row]
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuItem")
        if let name = selectedMenuItem?.name {
            fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        }
        let res = try! managedContext.fetch(fetchRequest) as! [MenuItem]
        if let menuItem = res.first {
            menuItem.addToRecipes(modifiedRecipe)
        }
        do {
            try self.context.save()
        } catch let error {
            print("Error save recipes in updateRecipe: \(error)")
        }
        
        selectedRecipes.removeAll()
        
        if let sMenuItem = selectedMenuItem {
            fetchRequest.predicate = NSPredicate(format: "name == %@", sMenuItem.name!)
        }
        
        let menuItemFetchResult = try! managedContext.fetch(fetchRequest) as! [MenuItem]
        if let menuItem = menuItemFetchResult.first {
            for item in menuItem.recipes as! Set<Recipe> {
                selectedRecipes.append(item)
            }
        }
        DispatchQueue.main.async {
            self.selectedRecipeTableView.reloadData()
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let textFieldText = textField.text {
            if textFieldText.count == 0 {
                isSearching = false
            } else {
                isSearching = true
            }
            filtredAllRecipes = allRecipes.filter{ $0.name?.range(of: textFieldText, options: [.caseInsensitive]) != nil
                || $0.name?.range(of: textFieldText, options: [.caseInsensitive]) != nil }
            currentCount = textFieldText.count
        }
        allRecipesTableView.reloadData()
    }
    
    class func instantiateViewController() -> RecipeSearchViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "RecipeSearchViewController") as! RecipeSearchViewController
    }
}

extension RecipeSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == selectedRecipeTableView {
            return selectedRecipes.count
        } else if tableView == allRecipesTableView {
            if isSearching {
                return filtredAllRecipes.count
            } else {
                return allRecipes.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == selectedRecipeTableView {
            let cell = selectedRecipeTableView.dequeueReusableCell(withIdentifier: "SelectedRecipeTableViewCell") as! SelectedRecipeTableViewCell
            cell.recipeNameLabel.text = selectedRecipes[indexPath.row].name
            return cell
        } else if tableView == allRecipesTableView {
            let cell = allRecipesTableView.dequeueReusableCell(withIdentifier: "AllRecipesTableViewCell") as! AllRecipesTableViewCell
            if isSearching {
                cell.recipeNameLabel.text = filtredAllRecipes[indexPath.row].name
            } else {
                cell.recipeNameLabel.text = allRecipes[indexPath.row].name
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == allRecipesTableView {
            selectedAllRecipeIndex = indexPath
            if indexPath.count > 0 {
                addToMenuButton.isEnabled = true
                addToMenuButton.setTitleColor(UIColor.white, for: .normal)
            } else {
                addToMenuButton.isEnabled = false
                addToMenuButton.setTitleColor(UIColor.gray, for: .disabled)
            }
        }
    }
}
