//
//  HomeViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-03-04.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var viewRecipeLibraryButton: UIButton! {
        didSet {
            viewRecipeLibraryButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var addNewRecipeButton: UIButton! {
        didSet {
            addNewRecipeButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var generateMenuButton: UIButton! {
        didSet {
            generateMenuButton.layer.cornerRadius = 8.0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func viewRecipeLibraryButtonAction(_ sender: Any) {
        let recipeListVC = RecipeListViewController.instantiateViewController()
        navigationController?.pushViewController(recipeListVC, animated: true)
    }
    
    @IBAction func addNewRecipeButtonAction(_ sender: Any) {
        let recipeLibraryVC = RecipeLibraryViewController.instantiateViewController()
        navigationController?.pushViewController(recipeLibraryVC, animated: true)
    }
    
    @IBAction func generateMenuButtonAction(_ sender: Any) {
        let mainVC = MenuItemViewController.instantiateViewController()
        navigationController?.pushViewController(mainVC, animated: true)
    }
}
