//
//  RecipeListViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-03-04.
//

import UIKit
import CoreData

class RecipeListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noRecipesLabel: UILabel!
    @IBOutlet weak var addNewRecipeButton: UIButton! {
        didSet {
            addNewRecipeButton.layer.cornerRadius = 8.0
        }
    }
    private var allRecipes = [Recipe]()
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchRecipes()
    }
    
    private func fetchRecipes() {
        allRecipes.removeAll()
        let recipesFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Recipe")
        let fetchedRecipes = try! context.fetch(recipesFetchRequest) as! [Recipe]
        for recipe in fetchedRecipes {
            if recipe.name != nil {
                allRecipes.append(recipe)
            }
        }
    }
    
    @IBAction func addNewRecipeButtonAction(_ sender: Any) {
        let addNewRecipeVC = AddNewRecipeViewController.instantiateViewController()
        addNewRecipeVC.callbackResult = {
            self.fetchRecipes()
            self.tableView.reloadData()
        }
        navigationController?.pushViewController(addNewRecipeVC, animated: true)
    }
    
    class func instantiateViewController() -> RecipeListViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "RecipeListViewController") as! RecipeListViewController
    }
}

extension RecipeListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
            self.context.delete(self.allRecipes[indexPath.row])
            do {
                try self.context.save()
            } catch let error {
                print("Error in trailingSwipeActionsConfigurationForRowAt: \(error)")
            }
            self.tableView.reloadData()
        }
        return UISwipeActionsConfiguration(actions: [action])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addNewRecipeVC = AddNewRecipeViewController.instantiateViewController()
        addNewRecipeVC.recipe = allRecipes[indexPath.row]
        addNewRecipeVC.isUpdatingRecipe = true
        addNewRecipeVC.callbackResult = {
            self.fetchRecipes()
            self.tableView.reloadData()
        }
        navigationController?.pushViewController(addNewRecipeVC, animated: true)
    }
}

extension RecipeListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRecipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "RecipeTableViewCell") as! RecipeTableViewCell
        if allRecipes.count > 0 {
            noRecipesLabel.isHidden = true
            cell.recipeNameLabel.text = allRecipes[safe: indexPath.row]?.name
        } else {
            noRecipesLabel.isHidden = false
        }
        return cell
    }
}
