//
//  RecipeLibraryViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-03-02.
//

import UIKit
import CoreData

class RecipeLibraryViewController: UIViewController {
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var exitWithoutSavingButton: UIButton! {
        didSet {
            exitWithoutSavingButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var recipeInstructionTextView: NumberedTextView! {
        didSet {
            recipeInstructionTextView.layer.borderWidth = 1.0
            recipeInstructionTextView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var ingredientTableView: UITableView!
    @IBOutlet weak var recipeTableView: UITableView!
    @IBOutlet weak var quantityTitleLabel: UILabel! {
        didSet {
            quantityTitleLabel.layer.borderWidth = 1.0
            quantityTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var unitTitleLabel: UILabel! {
        didSet {
            unitTitleLabel.layer.borderWidth = 1.0
            unitTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var ingredientTitleLabel: UILabel! {
        didSet {
            ingredientTitleLabel.layer.borderWidth = 1.0
            ingredientTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var recipeFileNameView: UIView! {
        didSet {
            recipeFileNameView.layer.borderWidth = 1.0
            recipeFileNameView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var recipeIngredientView: UIView! {
        didSet {
            recipeIngredientView.layer.borderWidth = 1.0
            recipeIngredientView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var recipeInstructionView: UIView! {
        didSet {
            recipeInstructionView.layer.borderWidth = 1.0
            recipeInstructionView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var recipeFileNameEditButton: UIButton! {
        didSet {
            recipeFileNameEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var recipeIngredientEditButton: UIButton! {
        didSet {
            recipeIngredientEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var recipeInstructionEditButton: UIButton! {
        didSet {
            recipeInstructionEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var ingredientServingsView: UIView! {
        didSet {
            ingredientServingsView.layer.borderWidth = 1.0
            ingredientServingsView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var ingredientServingLabel: UILabel!
    @IBOutlet weak var recipeFileNameLabel: UILabel!
    
    private var nutritionFact: NutritionFact?
    private var alertTextView = UITextView()
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private var ingredients = [Ingredient]()
    private var selectedRecipeIndexPath = IndexPath()
    private var recipes = [Recipe]()
    private var allRecipes = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allRecipes.removeAll()
        let recipesFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Recipe")
        let recipes = try! context.fetch(recipesFetchRequest) as! [Recipe]
        for recipe in recipes {
            if recipe.name != nil {
                allRecipes.append(recipe)
            }
        }
    }
    
    private func displayAlertWithTextView(title: String, message: String, textView: UITextView) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.autoresizesSubviews = true
        alertTextView = UITextView(frame: CGRect.zero)
        alertTextView.translatesAutoresizingMaskIntoConstraints = false
        
        alertTextView.text = textView.text
        let leadConstraint = NSLayoutConstraint(item: alert.view!, attribute: .leading, relatedBy: .equal, toItem: alertTextView, attribute: .leading, multiplier: 1.0, constant: -8.0)
        let trailConstraint = NSLayoutConstraint(item: alert.view!, attribute: .trailing, relatedBy: .equal, toItem: alertTextView, attribute: .trailing, multiplier: 1.0, constant: 8.0)
        
        let topConstraint = NSLayoutConstraint(item: alert.view!, attribute: .top, relatedBy: .equal, toItem: alertTextView, attribute: .top, multiplier: 1.0, constant: -64.0)
        let bottomConstraint = NSLayoutConstraint(item: alert.view!, attribute: .bottom, relatedBy: .equal, toItem: alertTextView, attribute: .bottom, multiplier: 1.0, constant: 64.0)
        alert.view.addSubview(alertTextView)
        NSLayoutConstraint.activate([leadConstraint, trailConstraint, topConstraint, bottomConstraint])
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
            textView.text = self.alertTextView.text
        }))
        
        // Move up UIAlertController when keyboard is present: https://stackoverflow.com/a/51098284/7536185
        alert.addTextField { field in
            field.translatesAutoresizingMaskIntoConstraints = false
            field.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        let inCntrlr =  alert.children[0].view!
        inCntrlr.removeFromSuperview()
        present(alert, animated: true)
    }
    
    private func isEntityAttributeExist(name: String, entityName: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        let res = try! context.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }
    
    private func updateRecipe(recipeName: String) {
        let modifiedRecipe = Recipe(context: self.context)
        modifiedRecipe.name = recipeName
        modifiedRecipe.instruction = recipeInstructionTextView.text
        if ingredients.count > 0 {
            for i in 0...ingredients.count-1 {
                modifiedRecipe.addToIngredient(ingredients[i])
            }
        }
        recipes.append(modifiedRecipe)
        do {
            try self.context.save()
        } catch let error {
            print("Error save recipes in updateRecipe: \(error)")
        }
        recipeTableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    private func displayEditablePopup(label: UILabel, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        var inputTextField: UITextField?
        
        alert.addTextField { (textField : UITextField!) -> Void in
            inputTextField = textField
            textField.text = label.text
        }
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            label.text = inputTextField?.text
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func exitWithoutSavingButtonAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        guard let recipeName = recipeFileNameLabel.text else { return }
        if recipeName.isEmpty || recipeInstructionTextView.text.isEmpty {
            let alert = UIAlertController(title: "Recipe \(recipeName.isEmpty ? "name" : "instruction") cannot be empty", message: "Please type in recipe \(recipeName.isEmpty ? "name" : "instruction")", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        } else {
            var modifiedRecipeName = recipeName
            if isEntityAttributeExist(name: recipeName, entityName: "Recipe") {
                var counter = 1
                while isEntityAttributeExist(name: modifiedRecipeName, entityName: "Recipe") {
                    modifiedRecipeName = recipeName + " (\(String(counter)))"
                    counter += 1
                }
            }
            let addRecipeAction : [String: () -> Void] = [ "Add Recipe" : { (
                self.updateRecipe(recipeName: modifiedRecipeName)
            ) }]
            let cancelAction : [String: () -> Void] = [ "Cancel" : { (
            ) }]
            let buttonActions = [addRecipeAction, cancelAction]
            self.showCustomAlertWith(name: "Recipe will be saved as '\(modifiedRecipeName)'", type: "Recipe will have \(ingredients.count) ingredients", price: "", description: "", itemimage: nil, actions: buttonActions)
        }
    }
    
    @IBAction func recipeFileNameEditButtonAction(_ sender: Any) {
        displayEditablePopup(label: recipeFileNameLabel, title: "Recipe", message: "Name")
    }
    
    @IBAction func ingredientEditButtonAction(_ sender: Any) {
        let ingredientSearchViewController = IngredientSearchViewController.instantiateViewController()
        ingredientSearchViewController.delegate = self
        ingredientSearchViewController.selectedIngredients = ingredients
        if let nutritionFact = nutritionFact {
            ingredientSearchViewController.nutritionFact = nutritionFact
            
        }
        if let servingSize = ingredientServingLabel.text {
            ingredientSearchViewController.serivingSizeInt = Int(servingSize) ?? 1
        }
        let navController = UINavigationController(rootViewController: ingredientSearchViewController)
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func recipeInstructionEditButtonAction(_ sender: Any) {
        displayAlertWithTextView(title: "Enter recipe instruction", message: "\n\n\n\n\n\n\n\n", textView: recipeInstructionTextView)
    }
    
    class func instantiateViewController() -> RecipeLibraryViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "RecipeLibraryViewController") as! RecipeLibraryViewController
    }
}

extension RecipeLibraryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == recipeTableView {
            selectedRecipeIndexPath = indexPath
            recipeInstructionTextView.text = allRecipes[indexPath.row].instruction
            recipeFileNameLabel.text = allRecipes[indexPath.row].name
            let recipeIngredients = allRecipes[indexPath.row].ingredient
            ingredients.removeAll()
            
            for item in recipeIngredients as! Set<Ingredient> {
                ingredients.append(item)
            }
            ingredientTableView.reloadData()
        }
    }
}

extension RecipeLibraryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == recipeTableView {
            return allRecipes.count
        } else if tableView == ingredientTableView {
            return ingredients.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == recipeTableView {
            let cell = recipeTableView.dequeueReusableCell(withIdentifier: "RecipeTableViewCell") as! RecipeTableViewCell
            if allRecipes.count > 0 {
                cell.recipeNameLabel.text = allRecipes[safe: indexPath.row]?.name
            }
            return cell
        } else if tableView == ingredientTableView {
            let cell = ingredientTableView.dequeueReusableCell(withIdentifier: "IngredientTableViewCell") as! IngredientTableViewCell
            
            if let unitString = ingredients[safe: indexPath.row]?.amount,
               let measurementString = ingredients[safe: indexPath.row]?.measurement,
               let nameString = ingredients[safe: indexPath.row]?.name {
                
                let formatter = NumberFormatter()
                formatter.minimumFractionDigits = 0
                formatter.maximumFractionDigits = 2
                formatter.numberStyle = .decimal
                
                cell.quantityLabel.text = "\(unitString)"
                cell.unitLabel.text = measurementString
                cell.ingredientLabel.text = nameString
                
            }
            return cell
        }
        return UITableViewCell()
    }
}

extension RecipeLibraryViewController: AddNewRecipeViewControllerDelegate {
    func sendSavedIngredients(servingSize: String, ingredients: [Ingredient], nutritionFact: NutritionFact) {
        self.ingredients = ingredients
        ingredientServingLabel.text = servingSize
        ingredientTableView.reloadData()
    }
}
