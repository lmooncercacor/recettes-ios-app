//
//  IngredientSearchViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-01-18.
//

import UIKit
import DropDown

class IngredientSearchViewController: UIViewController {
    @IBOutlet weak var dropDownServingView: UIView! {
        didSet {
            dropDownServingView.layer.borderWidth = 1.0
            dropDownServingView.layer.borderColor = UIColor.black.cgColor
        }
    }
    @IBOutlet weak var totalServingSize: UILabel!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var ingredientTableView: UITableView!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var nutritionFactsContainerView: UIView! {
        didSet {
            nutritionFactsContainerView.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var ingredientContainerView: UIView!
    @IBOutlet weak var exitButton: UIButton! {
        didSet {
            exitButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var nutritionFactsView: NutritionFactsView!
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    weak var delegate: AddNewRecipeViewControllerDelegate?
    private let dropDown = DropDown()
    private let ingredientRequestHandler = IngredientRequestHandler()
    private var selectedCommonFood : CommonFood?
    private var selectedBrandedFood : BrandedFood?
    private var selectedCell = SelectedIngredientTableViewCell()
    private var resultSearchController = UISearchController()
    private var filteredTableData : NutritionixFoodData = NutritionixFoodData(common: [], branded: [])
    private var foodSearchResults: NutritionixFoodData = NutritionixFoodData(common: [], branded: [])
    var selectedIngredients = [Ingredient]()
    var serivingSizeInt = Int()
    var nutritionFact = NutritionFact()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = []
        view.backgroundColor = .white
        
        resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.searchBar.barStyle = UIBarStyle.black
            controller.searchBar.barTintColor = UIColor.white
            controller.searchBar.backgroundColor = UIColor.clear
            self.searchTableView.tableHeaderView = controller.searchBar
            return controller
        })()
        
        if let searchBarText = resultSearchController.searchBar.text {
            ingredientRequestHandler.loadFoodList(using: searchBarText) { results in
                self.foodSearchResults = results
                self.searchTableView.reloadData()
            }
        }
        searchTableView.reloadData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        dropDownView.addGestureRecognizer(tap)
        dropDown.anchorView = dropDownView
        let numbers = Array(1...100).map {String($0)}
        dropDown.dataSource = numbers
        
        if serivingSizeInt == 1 {
            dropDown.selectRow(at: 1)
        } else {
            dropDown.selectRow(at: serivingSizeInt)
            totalServingSize.text = "\(numbers[serivingSizeInt])"
        }
        
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let _ = self else { return }
            self?.totalServingSize.text = "\(item)"
            if let totalServing = self?.totalServingSize.text {
                if let servingSize = Int(totalServing) {
                    self?.updateIngredientListServing(servingSize : servingSize)
                }
            }
        }
    }
    
    private func updateIngredientListServing(servingSize: Int) {
        for ingredient in selectedIngredients {
            ingredient.amount *= Float(servingSize)
        }
        ingredientTableView.reloadData()
    }
    
    private func updateNutritionFacts(_ results: FoodNutritionData) {
        if let servingWeights = results.foods.first?.servingWeightGms {
            nutritionFact.totalServingSizeInGrams += servingWeights
            DispatchQueue.main.async {
                self.nutritionFactsView.servingSize.text = "\(String(format: "%.1f", self.nutritionFact.totalServingSizeInGrams))g"
            }
        }
        if let calories = results.foods.first?.calories {
            nutritionFact.totalCaloriesFloat += calories
            DispatchQueue.main.async {
                self.nutritionFactsView.calorie.text = "\(String(format: "%.1f", self.nutritionFact.totalCaloriesFloat))"
            }
        }
        if let totalFat = results.foods.first?.totalFat {
            nutritionFact.totalFatInGrams += totalFat
            DispatchQueue.main.async {
                self.nutritionFactsView.totalFat.text = "\(String(format: "%.1f", self.nutritionFact.totalFatInGrams))g"
            }
        }
        if let saturatedFat = results.foods.first?.saturatedFat {
            nutritionFact.totalSatFatInGrams += saturatedFat
            DispatchQueue.main.async {
                self.nutritionFactsView.saturatedFat.text = "\(String(format: "%.1f", self.nutritionFact.totalSatFatInGrams))g"
            }
        }
        if let cholestrol = results.foods.first?.cholestrol {
            nutritionFact.totalCholesterolMgs += cholestrol
            DispatchQueue.main.async {
                self.nutritionFactsView.cholesterol.text = "\(String(format: "%.1f", self.nutritionFact.totalCholesterolMgs))mg"
            }
        }
        if let sodium = results.foods.first?.sodium {
            nutritionFact.totalSodiumMgs += sodium
            DispatchQueue.main.async {
                self.nutritionFactsView.sodium.text = "\(String(format: "%.1f", self.nutritionFact.totalSodiumMgs))mg"
            }
        }
        if let potassium = results.foods.first?.potassium {
            nutritionFact.totalPotassiumMgs += potassium
            DispatchQueue.main.async {
                self.nutritionFactsView.potassium.text = "\(String(format: "%.1f", self.nutritionFact.totalPotassiumMgs))mg"
            }
        }
        if let totalCarbohydrate = results.foods.first?.totalCarbohydrate {
            nutritionFact.totalCarbs += totalCarbohydrate
            DispatchQueue.main.async {
                self.nutritionFactsView.totalCarbohydrates.text = "\(String(format: "%.1f", self.nutritionFact.totalCarbs))g"
            }
        }
        if let dietaryFiber = results.foods.first?.dietaryFiber {
            nutritionFact.totalDietaryFibre += dietaryFiber
            DispatchQueue.main.async {
                self.nutritionFactsView.dietaryFibre.text = "\(String(format: "%.1f", self.nutritionFact.totalDietaryFibre))g"
            }
        }
        if let sugars = results.foods.first?.sugars {
            nutritionFact.totalSugarsGrams += sugars
            DispatchQueue.main.async {
                self.nutritionFactsView.sugars.text = "\(String(format: "%.1f", self.nutritionFact.totalSugarsGrams))g"
            }
        }
        if let protein = results.foods.first?.protein {
            nutritionFact.totalProteinGrams += protein
            DispatchQueue.main.async {
                self.nutritionFactsView.protein.text = "\(String(format: "%.1f", self.nutritionFact.totalProteinGrams))g"
            }
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        dropDown.show()
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        delegate?.sendSavedIngredients(servingSize: totalServingSize.text!, ingredients: selectedIngredients, nutritionFact: nutritionFact)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func exitButtonAction(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    class func instantiateViewController() -> IngredientSearchViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "IngredientSearchViewController") as! IngredientSearchViewController
    }
}

extension IngredientSearchViewController :  UITableViewDataSource {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == ingredientTableView {
            let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
                self.selectedIngredients.remove(at: indexPath.row)
                self.ingredientTableView.reloadData()
            }
            return UISwipeActionsConfiguration(actions: [action])
        }
        return UISwipeActionsConfiguration(actions: [])
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == searchTableView {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == searchTableView {
            if section == 0 {
                return "Common Food"
            } else if section == 1 {
                return "Branded Food"
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ingredientTableView {
            return selectedIngredients.count
        } else if tableView == searchTableView {
            return 5
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ingredientTableView {
            let cell = ingredientTableView.dequeueReusableCell(withIdentifier: "SelectedIngredientTableViewCell") as! SelectedIngredientTableViewCell
            cell.ingredientNameLabel.text = selectedIngredients[indexPath.row].name
            selectedCell = cell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.quantityCounter.value = Double(selectedIngredients[indexPath.row].amount)
            cell.quantityLabel.text = "\(selectedIngredients[indexPath.row].amount)"
            cell.unitLabel.text = selectedIngredients[indexPath.row].measurement
            DispatchQueue.global().async {
                if let url = URL(string: self.selectedIngredients[indexPath.row].imgUrl!) {
                    if let data = try? Data(contentsOf: url)  {
                        DispatchQueue.main.async {
                            cell.ingredientImageView.image = UIImage(data: data)
                        }
                    }
                }
            }
            return cell
        } else if tableView == searchTableView {
            let cell = searchTableView.dequeueReusableCell(withIdentifier: "SearchIngredientTableViewCell") as! SearchIngredientTableViewCell
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            cell.textLabel?.textAlignment = .left
            cell.textLabel?.textColor = UIColor.black
            cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
            if self.resultSearchController.isActive {
                if indexPath.section == 0 {
                    cell.textLabel?.text = foodSearchResults.common[safe: indexPath.row]?.foodName
                } else if indexPath.section == 1 {
                    if foodSearchResults.branded[safe: indexPath.row]?.foodName != nil || foodSearchResults.branded[safe: indexPath.row]?.brandName != nil {
                        cell.textLabel?.text = "\(foodSearchResults.branded[safe: indexPath.row]?.foodName ?? "") - \(foodSearchResults.branded[safe: indexPath.row]?.brandName ?? "")"
                    }
                }
            }
            return cell
        }
        return UITableViewCell()
    }
}

extension IngredientSearchViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == searchTableView {
            if indexPath.section == 0 {
                selectedCommonFood = foodSearchResults.common[safe: indexPath.row]
                if let commonFood = selectedCommonFood {
                    let ingredient = Ingredient(context: self.context)
                    ingredient.amount = Float(commonFood.servingQty!)
                    ingredient.measurement = commonFood.servingUnit
                    ingredient.name = commonFood.foodName
                    if let commonFoodPhoto = commonFood.photo {
                        ingredient.imgUrl = commonFoodPhoto.thumb
                    }
                    selectedIngredients.append(ingredient)
                    let commonFoodData = ["query": commonFood.foodName,
                                          "timezone": Calendar.current.timeZone.identifier,
                                          "locale": Locale.current.identifier]
                    ingredientRequestHandler.loadCommonFoodNutritionData(commonFoodData) { results in
                        self.updateNutritionFacts(results)
                    }
                }
            } else if indexPath.section == 1 {
                selectedBrandedFood = foodSearchResults.branded[safe: indexPath.row]
                if let brandedFood = selectedBrandedFood {
                    let ingredient = Ingredient(context: self.context)
                    ingredient.amount = Float(brandedFood.servingQty!)
                    ingredient.measurement = brandedFood.servingUnit
                    ingredient.name = brandedFood.foodName
                    if let brandedFoodPhoto = brandedFood.photo {
                        ingredient.imgUrl = brandedFoodPhoto.thumb
                    }
                    selectedIngredients.append(ingredient)
                    if let nixItemId = brandedFood.nixItemId {
                        ingredientRequestHandler.loadBrandedFoodNutritionData(nixItemId) { results in
                            self.updateNutritionFacts(results)
                        }
                    }
                }
            }
            resultSearchController.isActive = false
            ingredientTableView.reloadData()
        }
    }
}

extension IngredientSearchViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if self.resultSearchController.isActive {
            nutritionFactsContainerView.isHidden = true
            ingredientContainerView.isHidden = true
        } else {
            nutritionFactsContainerView.isHidden = false
            ingredientContainerView.isHidden = false
        }
        foodSearchResults.common.removeAll()
        if let searchBarText = resultSearchController.searchBar.text {
            ingredientRequestHandler.loadFoodList(using: searchBarText) { results in
                self.foodSearchResults = results
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
            }
        }
        DispatchQueue.main.async {
            self.searchTableView.reloadData()
        }
    }
}

extension IngredientSearchViewController: FoodTableViewCellDelegate {
    func quantifyCounter(cell: SelectedIngredientTableViewCell) {
        selectedIngredients[cell.tag].amount = Float(Int(cell.quantityCounter.value))
    }
    
    func unitSelector(cell: SelectedIngredientTableViewCell, unit: String) {
        if let indexPath = ingredientTableView.indexPath(for: cell){
            selectedIngredients[indexPath.row].measurement = unit
        }
    }
}
