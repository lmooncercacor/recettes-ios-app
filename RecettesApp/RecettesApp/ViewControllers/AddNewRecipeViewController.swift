//
//  AddNewRecipeViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-22.
//

import UIKit
import CoreData

protocol AddNewRecipeViewControllerDelegate: AnyObject {
    func sendSavedIngredients(servingSize: String, ingredients: [Ingredient], nutritionFact: NutritionFact)
}

class AddNewRecipeViewController: UIViewController {
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.layer.cornerRadius = 8.0
        }
        
    }
    @IBOutlet weak var deleteButton: UIButton! {
        didSet {
            deleteButton.layer.cornerRadius = 8.0
        }
        
    }
    @IBOutlet weak var recipeInstructionTextView: NumberedTextView! {
        didSet {
            recipeInstructionTextView.layer.borderWidth = 1.0
            recipeInstructionTextView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var quantityTitleLabel: UILabel! {
        didSet {
            quantityTitleLabel.layer.borderWidth = 1.0
            quantityTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var unitTitleLabel: UILabel! {
        didSet {
            unitTitleLabel.layer.borderWidth = 1.0
            unitTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var ingredientTitleLabel: UILabel! {
        didSet {
            ingredientTitleLabel.layer.borderWidth = 1.0
            ingredientTitleLabel.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
    @IBOutlet weak var recipeFileNameView: UIView! {
        didSet {
            recipeFileNameView.layer.borderWidth = 1.0
            recipeFileNameView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var recipeIngredientView: UIView!
    @IBOutlet weak var recipeInstructionView: UIView! {
        didSet {
            recipeInstructionView.layer.borderWidth = 1.0
            recipeInstructionView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var recipeFileNameEditButton: UIButton! {
        didSet {
            recipeFileNameEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var recipeIngredientEditButton: UIButton! {
        didSet {
            recipeIngredientEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var recipeInstructionEditButton: UIButton! {
        didSet {
            recipeInstructionEditButton.setTitle("", for: .normal)
        }
    }
    @IBOutlet weak var ingredientServingsView: UIView! {
        didSet {
            ingredientServingsView.layer.borderWidth = 1.0
            ingredientServingsView.layer.borderColor = UIColor.systemGray2.cgColor
        }
    }
    @IBOutlet weak var ingredientServingLabel: UILabel!
    @IBOutlet weak var recipeFileNameLabel: UILabel!
    
    private var alertTextView = UITextView()
    private var nutritionFact: NutritionFact?
    weak var delegate: AddNewRecipeViewControllerDelegate?
    private var ingredients = [Ingredient]()
    var selectedMenuItemIndexPath = IndexPath()
    private var qtyAlertViewTextField: UITextField?
    private var unitAlertViewTextField: UITextField?
    private var ingredientAlertViewTextField: UITextField?
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private let viewModel = MenuItemViewModel()
    var menuItems = [MenuItems]()
    var recipe : Recipe?
    var isUpdatingRecipe: Bool = false
    var callbackResult: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deleteButton.setTitle(isUpdatingRecipe ? "Delete" : "Exit", for: .normal)
        
        if let existRecipe = recipe {
            recipeInstructionTextView.text = existRecipe.instruction
            recipeFileNameLabel.text = existRecipe.name
            let recipeIngredients = existRecipe.ingredient
            ingredients.removeAll()
            
            for item in recipeIngredients as! Set<Ingredient> {
                ingredients.append(item)
            }
        }
    }
    
    private func updateIngredientListServing(servingSize: Int) {
        for ingredient in ingredients {
            ingredient.amount *= Float(servingSize)
        }
        tableView.reloadData()
    }
    
    private func addIngredientToTable() {
        let ingredient = Ingredient(context: self.context)
        
        if let qtyTextField = qtyAlertViewTextField?.text {
            let qtyText = String(qtyTextField.filter { !" \n\t\r".contains($0) })
            
            if !qtyText.isEmpty {
                
                if qtyText.contains("/") {
                    var op1 = Float()
                    var op2 = Float()
                    let comps = qtyText.components(separatedBy: "/")
                    op1 = NSString(string: comps[0]).floatValue
                    op2 = NSString(string: comps[1]).floatValue
                    ingredient.amount = Float(op1/op2)
                    
                } else if qtyText.contains(".") {
                    if let qty = Float(qtyText) {
                        ingredient.amount = qty
                    }
                } else {
                    if let qty = Float(qtyText) {
                        ingredient.amount = qty
                    }
                }
            }
        }
        ingredient.measurement = unitAlertViewTextField?.text
        ingredient.name = ingredientAlertViewTextField?.text
        ingredients.append(ingredient)
    }
    
    private func displayIngredientEditablePopup(title: String, message: String, quantity: Float?, unit: String?, ingredient: String?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField { (textField : UITextField!) -> Void in
            self.qtyAlertViewTextField = textField
            textField.placeholder = "Qty"
            textField.delegate = self
            textField.text = ""
        }
        alert.addTextField { (textField : UITextField!) -> Void in
            self.unitAlertViewTextField = textField
            textField.placeholder = "Unit"
            textField.delegate = self
            textField.text = ""
        }
        alert.addTextField { (textField : UITextField!) -> Void in
            self.ingredientAlertViewTextField = textField
            textField.placeholder = "Ingredient"
            textField.delegate = self
            textField.text = ""
        }
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            self.addIngredientToTable()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func displayEditablePopup(label: UILabel, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        var inputTextField: UITextField?
        
        alert.addTextField { (textField : UITextField!) -> Void in
            inputTextField = textField
            textField.text = label.text
        }
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler:{ (UIAlertAction)in
            label.text = inputTextField?.text
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func displayAlertWithTextView(title: String, message: String, textView: UITextView) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.autoresizesSubviews = true
        alertTextView = UITextView(frame: CGRect.zero)
        alertTextView.translatesAutoresizingMaskIntoConstraints = false
        
        alertTextView.text = textView.text
        let leadConstraint = NSLayoutConstraint(item: alert.view!, attribute: .leading, relatedBy: .equal, toItem: alertTextView, attribute: .leading, multiplier: 1.0, constant: -8.0)
        let trailConstraint = NSLayoutConstraint(item: alert.view!, attribute: .trailing, relatedBy: .equal, toItem: alertTextView, attribute: .trailing, multiplier: 1.0, constant: 8.0)
        
        let topConstraint = NSLayoutConstraint(item: alert.view!, attribute: .top, relatedBy: .equal, toItem: alertTextView, attribute: .top, multiplier: 1.0, constant: -64.0)
        let bottomConstraint = NSLayoutConstraint(item: alert.view!, attribute: .bottom, relatedBy: .equal, toItem: alertTextView, attribute: .bottom, multiplier: 1.0, constant: 64.0)
        alert.view.addSubview(alertTextView)
        NSLayoutConstraint.activate([leadConstraint, trailConstraint, topConstraint, bottomConstraint])
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
            textView.text = self.alertTextView.text
        }))
        
        // Move up UIAlertController when keyboard is present: https://stackoverflow.com/a/51098284/7536185
        alert.addTextField { field in
            field.translatesAutoresizingMaskIntoConstraints = false
            field.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        let inCntrlr =  alert.children[0].view!
        inCntrlr.removeFromSuperview()
        
        present(alert, animated: true)
    }
    
    private func updateRecipeChange(recipeName: String) {
        let recipeFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Recipe")
        if let recipeName = recipeFileNameLabel.text {
            recipeFetch.predicate = NSPredicate(format: "name == %@", recipeName)
        }
        let modifiedRecipe = Recipe(context: self.context)
        modifiedRecipe.name = recipeName
        modifiedRecipe.instruction = recipeInstructionTextView.text
        if ingredients.count > 0 {
            for i in 0...ingredients.count-1 {
                modifiedRecipe.addToIngredient(ingredients[i])
            }
        }
        do {
            try self.context.save()
        } catch let error {
            print("Error in updateRecipeChange: \(error)")
        }
    }
    
    private func isEntityAttributeExist(name: String, entityName: String) -> Bool {
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        let res = try! managedContext.fetch(fetchRequest)
        return res.count > 0 ? true : false
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        if isUpdatingRecipe {
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            if let selectedRecipe = recipe {
                context.delete(selectedRecipe)
            }
            do {
                try context.save()
            } catch let error {
                print("Error in deleteButtonAction: \(error)")
            }
            callbackResult?()
            navigationController?.popViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        if let recipeName = recipeFileNameLabel.text {
            var modifiedRecipeName = recipeName
            if isEntityAttributeExist(name: recipeName, entityName: "Recipe") {
                var counter = 1
                while isEntityAttributeExist(name: modifiedRecipeName, entityName: "Recipe") {
                    modifiedRecipeName = recipeName + " (\(String(counter)))"
                    counter += 1
                }
            }
            let modifiedRecipe = Recipe(context: self.context)
            modifiedRecipe.name = modifiedRecipeName
            modifiedRecipe.instruction = recipeInstructionTextView.text
            if ingredients.count > 0 {
                for i in 0...ingredients.count-1 {
                    modifiedRecipe.addToIngredient(ingredients[i])
                }
            }
            do {
                try self.context.save()
            } catch let error {
                print("Error save recipes in updateRecipe: \(error)")
            }
        }
        callbackResult?()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func recipeFileNameEditButtonAction(_ sender: Any) {
        displayEditablePopup(label: recipeFileNameLabel, title: "Recipe", message: "Name")
    }
    
    @IBAction func ingredientEditButtonAction(_ sender: Any) {
        let ingredientSearchViewController = IngredientSearchViewController.instantiateViewController()
        ingredientSearchViewController.delegate = self
        ingredientSearchViewController.selectedIngredients = ingredients
        if let nutritionFact = nutritionFact {
            ingredientSearchViewController.nutritionFact = nutritionFact
        }
        if let servingSize = ingredientServingLabel.text {
            ingredientSearchViewController.serivingSizeInt = Int(servingSize) ?? 1
        }
        let navController = UINavigationController(rootViewController: ingredientSearchViewController)
        present(navController, animated: true, completion: nil)
    }
    
    @IBAction func recipeInstructionEditButtonAction(_ sender: Any) {
        displayAlertWithTextView(title: "Enter recipe instruction", message: "\n\n\n\n\n\n\n\n", textView: recipeInstructionTextView)
    }
    
    class func instantiateViewController() -> AddNewRecipeViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "AddNewRecipeViewController") as! AddNewRecipeViewController
    }
}

extension AddNewRecipeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
            self.ingredients.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension AddNewRecipeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IngredientTableViewCell") as! IngredientTableViewCell
        if let unitString = ingredients[safe: indexPath.row]?.amount,
           let measurementString = ingredients[safe: indexPath.row]?.measurement,
           let nameString = ingredients[safe: indexPath.row]?.name {
            
            let formatter = NumberFormatter()
            formatter.minimumFractionDigits = 0
            formatter.maximumFractionDigits = 2
            formatter.numberStyle = .decimal
            
            cell.quantityLabel.text = "\(unitString)"
            cell.unitLabel.text = measurementString
            cell.ingredientLabel.text = nameString
        }
        return cell
    }
}

extension AddNewRecipeViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == qtyAlertViewTextField {
            let text: NSString = textField.text! as NSString
            let resultString = text.replacingCharacters(in: range, with: string)
            let allowedCharacters = CharacterSet(charactersIn:"/0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: resultString)
            
            if allowedCharacters.isSuperset(of: characterSet) {
                let textArray = resultString.components(separatedBy: "/")
                if resultString.first == "/" {
                    return false
                }
                if textArray.count > 2 { //Allow only one "."
                    return false
                }
                if textArray.count == 2 {
                    let lastString = textArray.last
                    if lastString!.count > 2 {//Check number of decimal places
                        return false
                    }
                }
            } else {
                return false
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textfield tag \(textField.tag)")
    }
}

extension AddNewRecipeViewController: AddNewRecipeViewControllerDelegate {
    func sendSavedIngredients(servingSize: String, ingredients: [Ingredient], nutritionFact: NutritionFact) {
        self.ingredients = ingredients
        ingredientServingLabel.text = servingSize
        tableView.reloadData()
    }
}
