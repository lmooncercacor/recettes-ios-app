//
//  MenuItemViewController.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-10-15.
//

import UIKit
import JTAppleCalendar
import CoreData

class MenuItemViewController: UIViewController {
    @IBOutlet weak var noMenuItemsLabel: UILabel!
    @IBOutlet weak var currentCalendarView: JTACMonthView!
    @IBOutlet weak var nextCalendarView: JTACMonthView!
    @IBOutlet weak var currentMonthLabel: UILabel!
    @IBOutlet weak var nextMonthLabel: UILabel!
    @IBOutlet weak var addNewMenuItemButton: UIButton! {
        didSet {
            addNewMenuItemButton.layer.cornerRadius = 8.0
        }
    }
    @IBOutlet weak var menuItemListView: UIView! {
        didSet {
            menuItemListView.layer.borderWidth = 1.0
            menuItemListView.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    @IBOutlet weak var tableView: UITableView!
    
    private var monthSize: MonthSize? = nil
    private var currentCalendar = Calendar.current
    private var nextCalendar = Calendar.current.date(byAdding: .month, value: 1, to: Date())
    private let formatter = DateFormatter()
    private var numberOfRows = 6
    private var hasStrictBoundaries = true
    private var generateInDates: InDateCellGeneration = .forAllMonths
    private var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private let moc = NSManagedObjectContext(concurrencyType:.mainQueueConcurrencyType)
    private let privateMOC = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    private var selectedCalendar = JTACMonthView()
    private var selectedDate = Date()
    private let viewModel = MenuItemViewModel()
    private var selectedDateFormat = ""
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noMenuItemsLabel.isHidden = true
        currentCalendarView.selectDates([Date()])
        
        self.currentCalendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfCurrentCalendar(from: visibleDates)
        }
        
        self.nextCalendarView.visibleDates {[unowned self] (visibleDates: DateSegmentInfo) in
            self.setupViewsOfNextCalendar(from: visibleDates)
        }
        currentCalendarView.isScrollEnabled = false
        nextCalendarView.isScrollEnabled = false
        currentCalendarView.allowsMultipleSelection = false
        nextCalendarView.allowsMultipleSelection = false
    }
    
    func setupViewsOfCurrentCalendar(from visibleDates: DateSegmentInfo) {
        guard let startDate = visibleDates.monthDates.first?.date else {
            return
        }
        let month = currentCalendar.dateComponents([.month], from: startDate).month!
        let monthName = DateFormatter().monthSymbols[(month-1) % 12]
        let year = currentCalendar.component(.year, from: startDate)
        currentMonthLabel.text = monthName + " " + String(year)
    }
    
    func setupViewsOfNextCalendar(from visibleDates: DateSegmentInfo) {
        if let nextCalendarMonth = nextCalendar {
            let calenderDate = Calendar.current.dateComponents([.day, .year, .month], from: nextCalendarMonth)
            
            let monthName = DateFormatter().monthSymbols[(calenderDate.month!-1) % 12]
            nextMonthLabel.text = monthName + " " + String(calenderDate.year!)
        }
    }
    
    func handleCurrentMonthCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
        handleCurrentMonthCellSelection(view: cell, cellState: cellState)
        handleCurrentMonthCellTextColor(view: cell, cellState: cellState)
    }
    
    func handleNextMonthCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
        handleNextMonthCellSelection(view: cell, cellState: cellState)
        handleCellNextMonthTextColor(view: cell, cellState: cellState)
    }
    
    func handleCellNextMonthTextColor(view: JTACDayCell?, cellState: CellState) {
        guard let nextMonthCell = view as? NextMonthCellView  else {
            return
        }
        if cellState.isSelected {
            nextMonthCell.dayLabel.textColor = .white
        } else {
            nextMonthCell.dayLabel.textColor = .black
        }
        nextMonthCell.isHidden = cellState.dateBelongsTo != .thisMonth
    }
    
    func handleNextMonthCellSelection(view: JTACDayCell?, cellState: CellState) {
        guard let nextMonthCell = view as? NextMonthCellView else {return }
        if cellState.isSelected {
            nextMonthCell.selectedView.layer.cornerRadius =  13
            nextMonthCell.selectedView.isHidden = false
            currentCalendarView.deselectAllDates()
        } else {
            nextMonthCell.selectedView.isHidden = true
        }
    }
    
    func handleCurrentMonthCellTextColor(view: JTACDayCell?, cellState: CellState) {
        guard let currentMonthCell = view as? CurrentMonthCellView  else {
            return
        }
        if cellState.isSelected {
            currentMonthCell.dayLabel.textColor = .white
        } else {
            currentMonthCell.dayLabel.textColor = .black
        }
        currentMonthCell.isHidden = cellState.dateBelongsTo != .thisMonth
    }
    
    func handleCurrentMonthCellSelection(view: JTACDayCell?, cellState: CellState) {
        guard let currentMonthCell = view as? CurrentMonthCellView else {return }
        if cellState.isSelected {
            currentMonthCell.selectedView.layer.cornerRadius =  13
            currentMonthCell.selectedView.isHidden = false
            nextCalendarView.deselectAllDates()
        } else {
            currentMonthCell.selectedView.isHidden = true
        }
    }
    
    @IBAction func addNewMenuItemButtonAction(_ sender: Any) {
        let generateMenuVC = GenerateMenuViewController.instantiateViewController()
        navigationController?.pushViewController(generateMenuVC, animated: true)
    }
    
    class func instantiateViewController() -> MenuItemViewController {
        let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(withIdentifier: "MenuItemViewController") as! MenuItemViewController
    }
}

// MARK : JTAppleCalendarDelegate
extension MenuItemViewController: JTACMonthViewDelegate, JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        if calendar == currentCalendarView {
            formatter.dateFormat = "yyyy MM dd"
            formatter.timeZone = currentCalendar.timeZone
            formatter.locale = currentCalendar.locale
            
            let components = currentCalendar.dateComponents([.year, .month, .day, .hour], from: Date())
            let range = currentCalendar.range(of: .day, in: .month, for: Date())!
            let startDate = formatter.date(from: "\(components.year ?? 9999) \(components.month ?? 99) 01")!
            let endDate = formatter.date(from: "\(components.year ?? 9999) \(components.month ?? 99) \(range.count)")!
            
            let parameters = ConfigurationParameters(startDate: startDate,
                                                     endDate: endDate,
                                                     numberOfRows: numberOfRows,
                                                     calendar: currentCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: .sunday,
                                                     hasStrictBoundaries: hasStrictBoundaries)
            return parameters
        } else {
            formatter.dateFormat = "yyyy MM dd"
            formatter.timeZone = currentCalendar.timeZone
            formatter.locale = currentCalendar.locale
            
            let startDate = formatter.date(from: "2022 2 1")!
            let endDate = formatter.date(from: "2022 2 28")!
            let parameters = ConfigurationParameters(startDate: startDate,
                                                     endDate: endDate,
                                                     numberOfRows: numberOfRows,
                                                     calendar: currentCalendar,
                                                     generateInDates: generateInDates,
                                                     generateOutDates: generateOutDates,
                                                     firstDayOfWeek: .sunday,
                                                     hasStrictBoundaries: hasStrictBoundaries)
            return parameters
        }
    }
    
    func configureVisibleCell(currentMonthCell: CurrentMonthCellView, cellState: CellState, date: Date, indexPath: IndexPath) {
        currentMonthCell.layer.borderColor = UIColor.lightGray.cgColor
        currentMonthCell.dayLabel.text = cellState.text
        if currentCalendar.isDateInToday(date) {
            currentMonthCell.backgroundColor = .clear
        } else {
            currentMonthCell.backgroundColor = .white
        }
        handleCurrentMonthCellConfiguration(cell: currentMonthCell, cellState: cellState)
        currentMonthCell.monthLabel.text = ""
    }
    
    func configureVisibleNextMonthCell(nextMonthCell: NextMonthCellView, cellState: CellState, date: Date, indexPath: IndexPath) {
        nextMonthCell.layer.borderColor = UIColor.lightGray.cgColor
        nextMonthCell.dayLabel.text = cellState.text
        if currentCalendar.isDateInToday(date) {
            nextMonthCell.backgroundColor = .clear
        } else {
            nextMonthCell.backgroundColor = .white
        }
        handleNextMonthCellConfiguration(cell: nextMonthCell, cellState: cellState)
        nextMonthCell.monthLabel.text = ""
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        // This function should have the same code as the cellForItemAt function
        if calendar == currentCalendarView {
            let currentMonthCell = cell as! CurrentMonthCellView
            configureVisibleCell(currentMonthCell: currentMonthCell, cellState: cellState, date: date, indexPath: indexPath)
        } else {
            let nextMonthCell = cell as! NextMonthCellView
            configureVisibleNextMonthCell(nextMonthCell: nextMonthCell, cellState: cellState, date: date, indexPath: indexPath)
        }
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        if calendar == currentCalendarView {
            let currentMonthCell = currentCalendarView.dequeueReusableCell(withReuseIdentifier: "CurrentMonthCellView", for: indexPath) as! CurrentMonthCellView
            configureVisibleCell(currentMonthCell: currentMonthCell, cellState: cellState, date: date, indexPath: indexPath)
            return currentMonthCell
        } else {
            let nextMonthCell = nextCalendarView.dequeueReusableCell(withReuseIdentifier: "NextMonthCellView", for: indexPath) as! NextMonthCellView
            configureVisibleNextMonthCell(nextMonthCell: nextMonthCell, cellState: cellState, date: date, indexPath: indexPath)
            return nextMonthCell
        }
    }
    
    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        if calendar == currentCalendarView {
            handleCurrentMonthCellConfiguration(cell: cell, cellState: cellState)
        } else {
            handleNextMonthCellConfiguration(cell: cell, cellState: cellState)
        }
    }
    
    private func fetchMenuItem() {
        viewModel.fetchAllMenuItems { (_) in
            let viewModelMenuItems = self.viewModel.menuItems
            var mItems = [MenuItem]()
            if viewModelMenuItems.count > 0 {
                for i in 0...viewModelMenuItems.count-1 {
                    let selectedMenuItem = viewModelMenuItems[i]
                    self.privateMOC.parent = self.moc
                    self.privateMOC.perform({
                        let menuItem = MenuItem(context: self.context)
                        menuItem.id = selectedMenuItem.id!
                        menuItem.name = selectedMenuItem.name
                        menuItem.menuItemDescription = selectedMenuItem.description
                        menuItem.type = selectedMenuItem.dishType
                        if let menuItemPrice = selectedMenuItem.price {
                            menuItem.price = (menuItemPrice as NSString).floatValue
                        }
                        mItems.append(menuItem)
                        do {
                            try self.privateMOC.save()
                        } catch {
                            fatalError("Error save menu items in didSelectDate: \(error)")
                        }
                    })
                }
            }
            DispatchQueue.main.async { [weak self] in
                if let menuItemsCount = self?.viewModel.menuItems.count {
                    self?.noMenuItemsLabel.isHidden = menuItemsCount > 0 ? true : false
                }
                self?.tableView.reloadData()
            }
        }
    }
    
    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        let currentCalendar = Calendar.current
        let clickedDate = currentCalendar.dateComponents([.year, .month, .day, .hour], from: date)
        selectedDate = date
        selectedCalendar = calendar
        var dateComponents1 = DateComponents()
        dateComponents1.year = clickedDate.year
        dateComponents1.day = clickedDate.day
        
        if calendar == currentCalendarView {
            dateComponents1.month = clickedDate.month
            handleCurrentMonthCellConfiguration(cell: cell, cellState: cellState)
        } else {
            if clickedDate.month == 12 {
                if let clickedYear = clickedDate.year {
                    dateComponents1.year = clickedYear + 1
                }
                dateComponents1.month = 1
            } else {
                if let clickedMonth = clickedDate.month {
                    dateComponents1.month = clickedMonth + 1
                }
            }
            handleNextMonthCellConfiguration(cell: cell, cellState: cellState)
        }
        
        if let year = clickedDate.year, let month = clickedDate.month, let day = clickedDate.day {
            selectedDateFormat = "\(year)-\(month)-\(day)"
            fetchMenuItem()
        }
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let date = range.start
        let month = currentCalendar.component(.month, from: date)
        formatter.dateFormat = "MMM"
        let header: JTACMonthReusableView
        if month % 2 > 0 {
            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "CalendarSectionHeaderView", for: indexPath)
            (header as! CalendarSectionHeaderView).title.text = formatter.string(from: date)
        } else {
            header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "CalendarSectionHeaderView", for: indexPath)
            (header as! CalendarSectionHeaderView).title.text = formatter.string(from: date)
        }
        return header
    }
}

extension MenuItemViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemsTableViewCell") as! MenuItemsTableViewCell
        cell.menuTitleLabel.text = viewModel.menuItems[safe: indexPath.row]?.name
        cell.delegate = self
        cell.selectionStyle = .none
        cell.tag = indexPath.row
        if viewModel.menuItems.count > 0 {
            noMenuItemsLabel.isHidden = true
        } else {
            noMenuItemsLabel.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tableView {
            let generateMenuVC = GenerateMenuViewController.instantiateViewController()
            generateMenuVC.menuItem = viewModel.menuItems[indexPath.row]
            navigationController?.pushViewController(generateMenuVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == tableView {
            let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completionHandler) in
                if let menuItemId = self.viewModel.menuItems[indexPath.row].id, let menuId = self.viewModel.menuItems[indexPath.row].menus?.first?.menu_id {
                    self.viewModel.deleteMenuItems(menuId: "\(menuId)", menuItemId: "\(menuItemId)", completion: { success in
                        self.fetchMenuItem()
                    })
                }
            }
            return UISwipeActionsConfiguration(actions: [action])
        }
        return UISwipeActionsConfiguration(actions: [])
    }
}

extension MenuItemViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
    }
}

extension MenuItemViewController: MenuItemsTableViewCellDelegate {
    func addRemoveButtonTapped(cell: MenuItemsTableViewCell) {
        
    }
}
