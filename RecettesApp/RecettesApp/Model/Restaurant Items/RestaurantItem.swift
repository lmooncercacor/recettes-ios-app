//
//  RestaurantItem.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-18.
//

import Foundation


//{"id":184393,
//    "name":"Spanish Chorizo Pizza",
//    "restaurant_id":29,
//    "dish_type":"Pizza",
//    "dish_type_org":null,
//    "meal_type":null,
//    "image_dir":"https://testamplifyadmin3b05b324adaf4e949a1a6958725090a124539-dev.s3-us-west-2.amazonaws.com/public/restaurant/Masimo_Cafe/",
//    "image_name":"carissa-gan-_0JpjeqtSyg-unsplash.jpg",
//    "nf_calories":1150,
//    "nf_protein":32,
//    "nf_total_carbohydrate":114,
//    "nf_total_fat":43,
//    "nf_dietary_fiber":6,
//    "nf_cholesterol":50,
//    "nf_p":null,
//    "nf_potassium":520,
//    "nf_saturated_fat":23,
//    "nf_sodium":2780,
//    "nf_sugars":35,
//    "serving_qty":1,
//    "serving_unit":null,
//    "serving_weight_grams":null,
//    "updated_at":"2022-02-09T16:21:20.000Z",
//    "created_at":"2022-01-19T15:23:28.000Z",
//    "nf_trans_fat":0,
//    "nf_calories_from_fat":425,
//    "rating":0,
//    "description":"Tomato Sauce, Bell Peppers, Red Onions, Queso Fresco, Mozzarella Cheese",
//    "has_addon":0,
//    "menu_id":null,
//    "menu_name":null,
//    "from_time":null,
//    "to_time":null,
//    "price":"7.750",
//    "brand_id":null,
//    "ref_name":null,
//    "insight":"red",
//    "ranking":3,
//
//    "modification":{
//        "nf_sodium":1390,
//        "nf_sugars":17.5,
//        "nf_protein":16,
//        "description":"Recommend saving 1/2 portion for a future meal",
//        "instruction":"No modification required for this menu item",
//        "nf_calories":575,
//        "nf_potassium":260,
//        "nf_total_fat":21,
//        "nf_trans_fat":0,
//        "nf_cholesterol":25,
//        "nf_dietary_fiber":3,
//        "nf_saturated_fat":11.5,
//        "nf_calories_from_fat":212.5,
//        "nf_total_carbohydrate":57
//    },
//    "wellness_claims":null,
//    "availability":null
//
//}
