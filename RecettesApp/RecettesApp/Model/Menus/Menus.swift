//
//  MenuChildren.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-12-21.
//

import Foundation

struct Menus  {
    var price : Double
    var menu_id : Int64
}

extension Menus: Codable {
    enum CodingKeys: String, CodingKey {
        case price = "price"
        case menu_id = "menu_id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        price = try values.decode(Double.self, forKey: .price)
        menu_id = try values.decode(Int64.self, forKey: .menu_id)
    }

    func encode(to encoder: Encoder) throws {
      var container = encoder.container(keyedBy: CodingKeys.self)
      try container.encode(price, forKey: .price)
      try container.encode(menu_id, forKey: .menu_id)
    }
}

