//
//  Menu.swift
//  RecettesApp
//
//  Created by Leo Moon on 2021-12-21.
//

import Foundation

struct MenuItems: Codable {
    var id: Int64?
    var name: String?
    var description: String?
    var price: String?
    var menus: [Menus]?
    var dishType: String?
    
    init(id: Int64?, name: String?, description: String?, price: String?, menus: [Menus]?, dishType: String?) {
        self.id   = id
        self.name = name
        self.description  = description
        self.price = price
        self.menus = menus
        self.dishType = dishType
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Int64.self, forKey: .id)
        name = try? values.decode(String.self, forKey: .name)
        description = try? values.decode(String.self, forKey: .description)
        price = try? values.decode(String.self, forKey: .price)
        menus = try? values.decode([Menus].self, forKey: .menus)
        dishType = try? values.decode(String.self, forKey: .dishType)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(description, forKey: .description)
        try container.encode(price, forKey: .price)
        try container.encode(menus, forKey: .menus)
        try container.encode(dishType, forKey: .dishType)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case price = "price"
        case menus = "menus"
        case dishType = "dish_type"
    }
}
