//
//  MenuId.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-01-06.
//

struct MenuId: Decodable {
    let id: Int
}
