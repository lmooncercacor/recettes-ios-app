//
//  FoodNutritionInfo.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct FoodNutritionInfo: Codable {
    let foodName: String?
    let brandName: String?
    let servingQuantity: Float?
    let servingUnit: String?
    let servingWeightGms: Double?
    let calories: Double?
    let totalFat: Double?
    let saturatedFat: Double?
    let cholestrol: Double?
    let sodium: Double?
    let totalCarbohydrate: Double?
    let dietaryFiber: Double?
    let sugars: Double?
    let protein: Double?
    let potassium: Double?
    let p: Double?
    let nixBrandName: String?
    let nixBrandId: String?
    let nixItemName: String?
    let nixItemId: String?
    let photo: FoodPhoto?
    
    private enum CodingKeys: String, CodingKey {
        case foodName = "food_name"
        case brandName = "brand_name"
        case servingQuantity = "serving_qty"
        case servingUnit = "serving_unit"
        case servingWeightGms = "serving_weight_grams"
        case calories = "nf_calories"
        case totalFat = "nf_total_fat"
        case saturatedFat = "nf_saturated_fat"
        case cholestrol = "nf_cholesterol"
        case sodium = "nf_sodium"
        case totalCarbohydrate = "nf_total_carbohydrate"
        case dietaryFiber = "nf_dietary_fiber"
        case sugars = "nf_sugars"
        case protein = "nf_protein"
        case potassium = "nf_potassium"
        case p = "nf_p"
        case nixBrandName = "nix_brand_name"
        case nixBrandId = "nix_brand_id"
        case nixItemName = "nix_item_name"
        case nixItemId = "nix_item_id"
        case photo = "photo"
    }
}
