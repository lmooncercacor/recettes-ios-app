//
//  NutritionixFoodData.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct NutritionixFoodData: Codable {
    var common: [CommonFood]
    let branded: [BrandedFood]
}
