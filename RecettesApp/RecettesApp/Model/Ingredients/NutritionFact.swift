//
//  NutritionFact.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct NutritionFact {
    var totalServingSizeInGrams = 0.0
    var totalCaloriesFloat = 0.0
    var totalFatInGrams = 0.0
    var totalSatFatInGrams = 0.0
    var totalCholesterolMgs = 0.0
    var totalSodiumMgs = 0.0
    var totalPotassiumMgs = 0.0
    var totalCarbs = 0.0
    var totalDietaryFibre = 0.0
    var totalSugarsGrams = 0.0
    var totalProteinGrams = 0.0
}
