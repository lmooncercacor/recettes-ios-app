//
//  BrandedFood.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct BrandedFood: Codable {
    let foodName: String
    let servingUnit: String?
    let nixBrandId: String?
    let brandNameItemName: String?
    let servingQty: Double?
    let nfCalories: Double
    let photo: FoodPhoto?
    let brandName: String?
    let region: Int
    let brandType: Int
    let nixItemId: String?
    let locale: String?
    let foodSearchType: FoodSearchType = .branded
    
    private enum CodingKeys: String, CodingKey {
        case foodName = "food_name"
        case servingUnit = "serving_unit"
        case nixBrandId = "nix_brand_id"
        case brandNameItemName = "brand_name_item_name"
        case servingQty = "serving_qty"
        case nfCalories = "nf_calories"
        case photo = "photo"
        case brandName = "brand_name"
        case region = "region"
        case brandType = "brand_type"
        case nixItemId = "nix_item_id"
        case locale = "locale"
    }
}
