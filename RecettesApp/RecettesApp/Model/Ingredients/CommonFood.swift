//
//  CommonFood.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct CommonFood: Codable {
    let foodName: String
    let servingUnit: String?
    let tagName: String?
    let servingQty: Double?
    let commonType: Int?
    let tagId: String?
    let photo: FoodPhoto?
    let locale: String?
    let foodSearchType: FoodSearchType = .common
    
    private enum CodingKeys: String, CodingKey {
        case foodName = "food_name"
        case servingUnit = "serving_unit"
        case tagName = "tag_name"
        case servingQty = "serving_qty"
        case commonType = "common_type"
        case tagId = "tag_id"
        case photo = "photo"
        case locale = "locale"
    }
}
