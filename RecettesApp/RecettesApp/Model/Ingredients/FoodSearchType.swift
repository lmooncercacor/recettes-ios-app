//
//  FoodSearchType.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

enum FoodSearchType: String, Codable {
    case common
    case branded
    case none
}
