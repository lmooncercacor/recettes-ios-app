//
//  FoodNutritionData.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct FoodNutritionData: Codable {
    let foods: [FoodNutritionInfo]
}
