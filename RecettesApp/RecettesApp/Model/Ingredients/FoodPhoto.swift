//
//  FoodPhoto.swift
//  RecettesApp
//
//  Created by Leo Moon on 2022-02-10.
//

struct FoodPhoto: Codable {
    let thumb: String?
}
